package com.snapsoft.demo.componentes

import com.snapsoft.demo.domains.seguridad.Usuario
import com.snapsoft.demo.services.SeguridadService
import com.snapsoft.demo.utileria.Constantes
import com.vaadin.grails.Grails
import com.vaadin.server.VaadinSession
import com.vaadin.ui.*

/**
 * Created by aluis on 5/22/14.
 */
class NuevoUsuario extends Window {

    private VerticalLayout mainLayout
    private HorizontalLayout hlControles

    private OptionGroup ogAdministrador
    private Button btnAceptar, btnCancelar
    private TextField tfUsername, tfNombre, tfApellido
    private PasswordField pfPassword

    private Usuario usuarioActivo

    public NuevoUsuario() {
        usuarioActivo = (Usuario) VaadinSession.current.session.getAttribute(Constantes.USER_LOGIN)

        mainLayout = new VerticalLayout()
        mainLayout.setMargin(true)
        mainLayout.setSpacing(true)

        buildLayout()

        setContent(mainLayout)
        center()
    }

    private void buildLayout() {
        tfUsername = new TextField("Username:")
        tfUsername.setRequired(true)

        pfPassword = new PasswordField("Password:")
        pfPassword.setRequired(true)

        tfNombre = new TextField("Nombre:")
        tfNombre.setRequired(true)

        tfApellido = new TextField("Apellido:")
        tfApellido.setRequired(true)

        ogAdministrador = new OptionGroup("Administrador?")
        ogAdministrador.setRequired(true)
        ogAdministrador.addItem("Si")
        ogAdministrador.addItem("No")

        hlControles = new HorizontalLayout()

        btnAceptar = new Button("Aceptar")
        btnAceptar.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                if (tfUsername.isValid() && pfPassword.isValid() && tfNombre.isValid() && tfApellido.isValid() && ogAdministrador.isValid()) {
                    Usuario u = new Usuario()
                    u.setUsername(tfUsername.getValue())
                    u.setPassword(pfPassword.getValue().encodeAsSHA256())
                    u.setNombre(tfNombre.getValue())
                    u.setApellido(tfApellido.getValue())
                    if (ogAdministrador.getValue().toString().equals("Si")) {
                        u.setAdministrador(true)
                    } else {
                        u.setAdministrador(false)
                    }
                    Usuario valor = Grails.get(SeguridadService).crearUsuario(u, usuarioActivo)
                    if (valor) {
                        Notification.show("Usuario Creado", "El usuario se creó con exito.", Notification.Type.TRAY_NOTIFICATION)
                        close()
                    } else {
                        Notification.show("Error Creando Usuario", "Error al crear el usuario.", Notification.Type.WARNING_MESSAGE)
                    }
                }
            }
        })

        btnCancelar = new Button("Cancelar")
        btnCancelar.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                close()
            }
        })

        hlControles.addComponent(btnAceptar)
        hlControles.addComponent(btnCancelar)

        mainLayout.addComponent(tfUsername)
        mainLayout.addComponent(pfPassword)
        mainLayout.addComponent(tfNombre)
        mainLayout.addComponent(tfApellido)
        mainLayout.addComponent(ogAdministrador)
        mainLayout.addComponent(hlControles)
    }
}
