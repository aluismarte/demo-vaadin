package com.snapsoft.demo.componentes

import com.snapsoft.demo.domains.seguridad.Usuario
import com.snapsoft.demo.utileria.Constantes
import com.vaadin.server.Page
import com.vaadin.server.VaadinServlet
import com.vaadin.server.VaadinSession
import com.vaadin.ui.Alignment
import com.vaadin.ui.Button
import com.vaadin.ui.HorizontalLayout
import com.vaadin.ui.Label

/**
 * Created by aluis on 5/21/14.
 */
class BarraEstado extends HorizontalLayout {

    private HorizontalLayout hlControles
    private Label saludo
    private Button btnSalir

    private Usuario usuarioActivo

    public BarraEstado() {
        setSizeFull()
        setSpacing(true)

        usuarioActivo = (Usuario) VaadinSession.current.session.getAttribute(Constantes.USER_LOGIN)

        buildLayout()
    }

    private void buildLayout() {
        hlControles = new HorizontalLayout()
        hlControles.setSpacing(true)

        saludo = new Label("Hola " + usuarioActivo.getNombre())
        btnSalir = new Button("Cerrar Sección")
        btnSalir.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                VaadinSession.current.session.invalidate()
                Page.getCurrent().setLocation(VaadinServlet.getCurrent().getServletContext().getContextPath())
            }
        })

        hlControles.addComponent(saludo)
        hlControles.addComponent(btnSalir)

        addComponent(hlControles)
        setComponentAlignment(hlControles, Alignment.MIDDLE_RIGHT)
    }
}
