package com.snapsoft.demo.componentes

import com.snapsoft.demo.domains.compra.ItemCanasta
import com.snapsoft.demo.domains.seguridad.Usuario
import com.snapsoft.demo.services.CompraService
import com.snapsoft.demo.utileria.Constantes
import com.vaadin.grails.Grails
import com.vaadin.server.VaadinSession
import com.vaadin.ui.*

/**
 * Created by aluis on 5/22/14.
 */
class NuevoItemCanasta extends Window {

    private VerticalLayout mainLayout
    private HorizontalLayout hlControles

    private Button btnAceptar, btnCancelar
    private TextField tfNombre, tfDescripcion

    private Usuario usuarioActivo

    public NuevoItemCanasta() {
        usuarioActivo = (Usuario) VaadinSession.current.session.getAttribute(Constantes.USER_LOGIN)

        mainLayout = new VerticalLayout()
        mainLayout.setMargin(true)
        mainLayout.setSpacing(true)

        buildLayout()

        setContent(mainLayout)
        center()
    }

    private void buildLayout() {
        tfNombre = new TextField("Nombre:")
        tfNombre.setRequired(true)

        tfDescripcion = new TextField("Descripcio:")
        tfDescripcion.setRequired(true)

        hlControles = new HorizontalLayout()

        btnAceptar = new Button("Aceptar")
        btnAceptar.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                if (tfNombre.isValid() && tfDescripcion) {
                    ItemCanasta ic = new ItemCanasta()
                    ic.setNombre(tfNombre.getValue())
                    ic.setDescripcion(tfDescripcion.getValue())
                    ItemCanasta valor = Grails.get(CompraService).crearItemCanasta(ic, usuarioActivo)
                    if (valor) {
                        Notification.show("Item Creado", "El item se creó con exito.", Notification.Type.TRAY_NOTIFICATION)
                        close()
                    } else {
                        Notification.show("Error Creando Item", "Error al crear el item.", Notification.Type.WARNING_MESSAGE)
                    }
                }
            }
        })

        btnCancelar = new Button("Cancelar")
        btnCancelar.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                close()
            }
        })

        hlControles.addComponent(btnAceptar)
        hlControles.addComponent(btnCancelar)

        mainLayout.addComponent(tfNombre)
        mainLayout.addComponent(tfDescripcion)
        mainLayout.addComponent(hlControles)
    }
}
