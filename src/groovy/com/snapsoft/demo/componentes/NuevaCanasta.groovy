package com.snapsoft.demo.componentes

import com.snapsoft.demo.domains.compra.Canasta
import com.snapsoft.demo.domains.compra.ItemCanasta
import com.snapsoft.demo.domains.seguridad.Usuario
import com.snapsoft.demo.services.CompraService
import com.snapsoft.demo.utileria.Constantes
import com.vaadin.event.ShortcutAction
import com.vaadin.grails.Grails
import com.vaadin.server.VaadinSession
import com.vaadin.ui.*

/**
 * Created by aluis on 5/22/14.
 */
class NuevaCanasta extends Window {

    private VerticalLayout mainLayout
    private HorizontalLayout hlControles, hlItems

    private Button btnAceptar, btnCancelar, btnAgregarItem
    private TextField tfNombre, tfDescripcion
    private ComboBox cbItemsCanasta
    private Table tablaItemsCanasta

    private Usuario usuarioActivo
    private List<ItemCanasta> itemsSeleccionados = new ArrayList<ItemCanasta>()

    public NuevaCanasta() {
        usuarioActivo = (Usuario) VaadinSession.current.session.getAttribute(Constantes.USER_LOGIN)

        mainLayout = new VerticalLayout()
        mainLayout.setMargin(true)
        mainLayout.setSpacing(true)

        buildLayout()

        setContent(mainLayout)
        center()
    }

    private void buildLayout() {
        tfNombre = new TextField("Nombre:")
        tfNombre.setRequired(true)

        tfDescripcion = new TextField("Descripcio:")
        tfDescripcion.setRequired(true)

        hlItems = new HorizontalLayout()

        cbItemsCanasta = new ComboBox("Item")
        cbItemsCanasta.setRequired(true)
        llenarCBItems()

        btnAgregarItem = new Button("Agregar Item")
        btnAgregarItem.setClickShortcut(ShortcutAction.KeyCode.ENTER)
        btnAgregarItem.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                Long valor = cbItemsCanasta.getValue().toString().toLong()
                if (valor) {
                    ItemCanasta ic = ItemCanasta.get(valor)
                    agregarItemCanasta(ic)
                    actualizarTabla()
                    cbItemsCanasta.select(null)
                }
            }
        })

        hlItems.addComponent(cbItemsCanasta)
        hlItems.addComponent(btnAgregarItem)

        tablaItemsCanasta = new Table("Items")
        tablaItemsCanasta.setSizeFull()
        tablaItemsCanasta.setSelectable(true)
        tablaItemsCanasta.setImmediate(true)
        tablaItemsCanasta.addContainerProperty("nombre", String.class, null, "Nombre", null, null)
        tablaItemsCanasta.addContainerProperty("descripcion", String.class, null, "Descripcion", null, null)

        hlControles = new HorizontalLayout()

        btnAceptar = new Button("Aceptar")
        btnAceptar.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                if (tfNombre.isValid() && tfDescripcion.isValid()) {
                    Canasta c = new Canasta()
                    c.setNombre(tfNombre.getValue())
                    c.setDescripcion(tfDescripcion.getValue())
                    c.setListaItems(itemsSeleccionados.toSet())
                    Canasta valor = Grails.get(CompraService).crearCanasta(c, usuarioActivo)
                    if (valor) {
                        Notification.show("Canasta Creada", "La canasta se creó con exito.", Notification.Type.TRAY_NOTIFICATION)
                        close()
                    } else {
                        Notification.show("Error Creando Canasta", "Error al crear la Canasta.", Notification.Type.WARNING_MESSAGE)
                    }
                }
            }
        })

        btnCancelar = new Button("Cancelar")
        btnCancelar.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                close()
            }
        })

        hlControles.addComponent(btnAceptar)
        hlControles.addComponent(btnCancelar)

        mainLayout.addComponent(tfNombre)
        mainLayout.addComponent(tfDescripcion)
        mainLayout.addComponent(hlItems)
        mainLayout.addComponent(tablaItemsCanasta)
        mainLayout.addComponent(hlControles)
    }

    private void llenarCBItems() {
        cbItemsCanasta.removeAllItems()
        List<ItemCanasta> itemCanastaList = Grails.get(CompraService).listarItemCanasta()
        for (ItemCanasta ic : itemCanastaList) {
            cbItemsCanasta.addItem(ic.id)
            cbItemsCanasta.setItemCaption(ic.id, ic.nombre)
        }
    }

    private void agregarItemCanasta(ItemCanasta itemCanasta) {
        if (!itemsSeleccionados.contains(itemCanasta)) {
            itemsSeleccionados.add(itemCanasta)
        }
    }

    private void actualizarTabla() {
        tablaItemsCanasta.removeAllItems()
        for (ItemCanasta ic : itemsSeleccionados) {
            tablaItemsCanasta.addItem([ic.nombre, ic.descripcion] as Object[], ic)
        }
    }
}
