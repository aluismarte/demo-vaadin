package com.snapsoft.demo.componentes

import com.snapsoft.demo.domains.seguridad.Usuario
import com.snapsoft.demo.interfaces.ICallBackUsuario
import com.snapsoft.demo.principales.CreacionCanastas
import com.snapsoft.demo.principales.CreacionItemsCanastas
import com.snapsoft.demo.principales.CreacionUsuarios
import com.snapsoft.demo.principales.Principal
import com.snapsoft.demo.services.SeguridadService
import com.snapsoft.demo.utileria.Constantes
import com.vaadin.event.ShortcutAction
import com.vaadin.grails.Grails
import com.vaadin.server.VaadinSession
import com.vaadin.ui.*
import com.vaadin.ui.themes.Reindeer

/**
 * Created by aluis on 5/21/14.
 */
class Login extends Window {

    VerticalLayout mainLayout
    HorizontalLayout hlCampos, hlBotones

    private Label lPrometeo
    private TextField tfUsuario
    private PasswordField pfClave

    private Button btnLogin, btnOlvideClave

    private Usuario usuarioActivo = null
    private ICallBackUsuario callback
//    private String COOKIE_NAME = "prometeoC"

    public Login(ICallBackUsuario callback) {
        this.callback = callback
        usuarioActivo = (Usuario) VaadinSession.current.session.getAttribute(Constantes.USER_LOGIN)

        if (usuarioActivo != null) {
            callback.onDialogResult(usuarioActivo)
            if (usuarioActivo.getAdministrador()) {
                CreacionUsuarios creacionUsuarios = new CreacionUsuarios()
                Principal.contenido.addTabPanel("Usuarios", creacionUsuarios)
            } else {
                CreacionCanastas creacionCanastas = new CreacionCanastas()
                CreacionItemsCanastas creacionItemsCanastas = new CreacionItemsCanastas()
                Principal.contenido.addTabPanel("Canastas", creacionCanastas)
                Principal.contenido.addTabPanel("Items Canastas", creacionItemsCanastas)
            }
        } else {
            mainLayout = new VerticalLayout()
            mainLayout.setSpacing(true)
            mainLayout.setMargin(true)
            mainLayout.addStyleName("")

            center()
            setClosable(false)
            setResizable(false)
            addStyleName("login")

            setContent(buildLayout())
        }
    }

    private Component buildLayout() {
        // Creo el titulo
        lPrometeo = new Label("Prometeo")
        lPrometeo.setSizeFull()
        lPrometeo.addStyleName("h2")

        // Creo los campos de data
        hlCampos = new HorizontalLayout()
        hlCampos.setSizeFull()
        hlCampos.setSpacing(true)

        tfUsuario = new TextField("Usuario:")
        tfUsuario.focus()
        pfClave = new PasswordField("Contraseña:")

        hlCampos.addComponent(tfUsuario)
        hlCampos.addComponent(pfClave)

        // Configuro los botones
        hlBotones = new HorizontalLayout()
        hlBotones.setSizeFull()
        hlBotones.setSpacing(true)

        btnLogin = new Button("Login")
        btnLogin.setClickShortcut(ShortcutAction.KeyCode.ENTER)
        btnLogin.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                usuarioActivo = Grails.get(SeguridadService).autentificarUsuario(tfUsuario.getValue(), pfClave.getValue())
                if (usuarioActivo != null) {
                    VaadinSession.current.session.setAttribute(Constantes.USER_LOGIN, usuarioActivo)
                    callback.onDialogResult(usuarioActivo)
                    if (usuarioActivo.getAdministrador()) {
                        CreacionUsuarios creacionUsuarios = new CreacionUsuarios()
                        Principal.contenido.addTabPanel("Usuarios", creacionUsuarios)
                    } else {
                        CreacionCanastas creacionCanastas = new CreacionCanastas()
                        CreacionItemsCanastas creacionItemsCanastas = new CreacionItemsCanastas()
                        Principal.contenido.addTabPanel("Canastas", creacionCanastas)
                        Principal.contenido.addTabPanel("Items Canastas", creacionItemsCanastas)

                    }
                    close()
                } else {
                    Notification.show("Login Error", "Verifica tu usuario y contraseña", Notification.Type.WARNING_MESSAGE)
                }
            }
        })

        btnOlvideClave = new Button("Olvide la Contraseña")
        btnOlvideClave.addStyleName(Reindeer.BUTTON_LINK)

        hlBotones.addComponent(btnLogin)
        hlBotones.addComponent(btnOlvideClave)
        hlBotones.setComponentAlignment(btnOlvideClave, Alignment.MIDDLE_RIGHT)

        mainLayout.addComponent(lPrometeo)
        mainLayout.addComponent(hlCampos)
        mainLayout.addComponent(hlBotones)

        return mainLayout
    }

}
