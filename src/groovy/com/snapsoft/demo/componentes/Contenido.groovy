package com.snapsoft.demo.componentes

import com.vaadin.ui.Component
import com.vaadin.ui.TabSheet
import com.vaadin.ui.VerticalLayout

/**
 * Created by aluis on 5/22/14.
 */
class Contenido extends VerticalLayout {

    private TabSheet tabSheet

    public Contenido() {
        setSizeFull()
        setSpacing(true)
        setMargin(true)
        buildLayout()
    }

    private void buildLayout() {
        tabSheet = new TabSheet()
        tabSheet.setSizeFull()

        addComponent(tabSheet)
    }

    public void addTabPanel(String nombre, Component componente) {
        Boolean existe = false
        for (int i = 0; i < tabSheet.getComponentCount(); i++) {
            if (tabSheet.getTab(i).caption.equals(nombre)) {
                existe = true
                tabSheet.setSelectedTab(i)
            }
        }
        if (!existe) {
            TabSheet.Tab tab = tabSheet.addTab(componente, nombre)
//            tab.setClosable(true)
            tab.setClosable(false)
            tabSheet.setSelectedTab(tab)
        }
    }
}
