package com.snapsoft.demo.interfaces

import com.snapsoft.demo.domains.seguridad.Usuario

/**
 * Created by aluis on 5/21/14.
 */
public interface ICallBackUsuario {
    public void onDialogResult(Usuario usuario)
}