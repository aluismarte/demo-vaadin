package com.snapsoft.demo.utileria

/**
 * Created by aluis on 5/21/14.
 */
class Constantes {
    public static final String NAME_COOKIE = "prometeo_cookie"
    public static final String USER_LOGIN = "usuario_logueado"
    public static final String USER_PRIVILEGES = "usuario_privilegios"
}
