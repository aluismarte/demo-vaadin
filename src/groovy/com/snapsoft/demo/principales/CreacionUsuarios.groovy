package com.snapsoft.demo.principales

import com.snapsoft.demo.componentes.NuevoUsuario
import com.snapsoft.demo.domains.seguridad.Usuario
import com.snapsoft.demo.services.SeguridadService
import com.snapsoft.demo.utileria.Constantes
import com.vaadin.grails.Grails
import com.vaadin.server.VaadinSession
import com.vaadin.ui.*

/**
 * Created by aluis on 5/22/14.
 */
class CreacionUsuarios extends Panel {

    private VerticalLayout mainLayout
    private HorizontalLayout hlControles

    private Button btnNuevo, btnDeshabilitar
    private Table tablaUsuarios

    private Usuario usuarioActivo

    public CreacionUsuarios() {
        usuarioActivo = (Usuario) VaadinSession.current.session.getAttribute(Constantes.USER_LOGIN)
        setSizeFull()
        mainLayout = new VerticalLayout()
        mainLayout.setSizeFull()

        buildLayout()

        setContent(mainLayout)
    }

    private void buildLayout() {
        hlControles = new HorizontalLayout()
        hlControles.setSpacing(true)
        hlControles.setMargin(true)

        btnNuevo = new Button("Nuevo Usuario")
        btnNuevo.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                NuevoUsuario nu = new NuevoUsuario()
                nu.addCloseListener(new Window.CloseListener() {
                    @Override
                    void windowClose(Window.CloseEvent closeEvent) {
                        actualizarTabla()
                    }
                })
                getUI().addWindow(nu)
            }
        })

        btnDeshabilitar = new Button("Habilitar/Deshabilitar")
        btnDeshabilitar.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                Usuario valor = (Usuario) tablaUsuarios.getValue()
                if (valor) {
                    Usuario u = Grails.get(SeguridadService).deshabilitarUsuario(valor, usuarioActivo)
                    if (u) {
                        Notification.show("Usuario Cambiado", "Se cambio el estado de habilitado.", Notification.Type.TRAY_NOTIFICATION)
                    } else {
                        Notification.show("Error Usuario", "Error al cambiar el estado de habilitado.", Notification.Type.WARNING_MESSAGE)
                    }
                    actualizarTabla()
                }
            }
        })

        hlControles.addComponent(btnNuevo)
        hlControles.addComponent(btnDeshabilitar)

        tablaUsuarios = new Table("Usuarios")
        tablaUsuarios.setSizeFull()
        tablaUsuarios.setSelectable(true)
        tablaUsuarios.setImmediate(true)
        tablaUsuarios.addContainerProperty("username", String.class, null, "Username", null, null)
        tablaUsuarios.addContainerProperty("nombre", String.class, null, "Nombre", null, null)
        tablaUsuarios.addContainerProperty("apellido", String.class, null, "Apellido", null, null)
        tablaUsuarios.addContainerProperty("administrador", Boolean.class, null, "Administrador", null, null)
        tablaUsuarios.addContainerProperty("habilitado", Boolean.class, null, "Habilitado", null, null)

        actualizarTabla()

        mainLayout.addComponent(hlControles)
        mainLayout.addComponent(tablaUsuarios)
        mainLayout.setComponentAlignment(hlControles, Alignment.MIDDLE_CENTER)
    }

    private void actualizarTabla() {
        tablaUsuarios.removeAllItems()
        List<Usuario> usuariosList = Grails.get(SeguridadService).listarUsuarios()
        for (Usuario u : usuariosList) {
            tablaUsuarios.addItem([u.username, u.nombre, u.apellido, u.administrador, u.habilitado] as Object[], u)
        }
    }
}
