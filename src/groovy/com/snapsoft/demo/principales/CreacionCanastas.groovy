package com.snapsoft.demo.principales

import com.snapsoft.demo.componentes.NuevaCanasta
import com.snapsoft.demo.componentes.NuevoItemCanasta
import com.snapsoft.demo.domains.compra.Canasta
import com.snapsoft.demo.domains.seguridad.Usuario
import com.snapsoft.demo.services.CompraService
import com.snapsoft.demo.utileria.Constantes
import com.vaadin.grails.Grails
import com.vaadin.server.VaadinSession
import com.vaadin.ui.*

/**
 * Created by aluis on 5/22/14.
 */
class CreacionCanastas extends Panel {

    private VerticalLayout mainLayout
    private HorizontalLayout hlControles

    private Button btnNuevo, btnDeshabilitar, btnNuevoItemCanasta
    private Table tablaCanastas

    private Usuario usuarioActivo

    public CreacionCanastas() {
        usuarioActivo = (Usuario) VaadinSession.current.session.getAttribute(Constantes.USER_LOGIN)
        setSizeFull()
        mainLayout = new VerticalLayout()
        mainLayout.setSizeFull()

        buildLayout()

        setContent(mainLayout)
    }

    private void buildLayout() {
        hlControles = new HorizontalLayout()
        hlControles.setSpacing(true)
        hlControles.setMargin(true)

        btnNuevo = new Button("Nueva Canasta")
        btnNuevo.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                NuevaCanasta nc = new NuevaCanasta()
                nc.addCloseListener(new Window.CloseListener() {
                    @Override
                    void windowClose(Window.CloseEvent closeEvent) {
                        actualizarTabla()
                    }
                })
                getUI().addWindow(nc)
            }
        })

        btnDeshabilitar = new Button("Habilitar/Deshabilitar")
        btnDeshabilitar.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                Canasta valor = (Canasta) tablaCanastas.getValue()
                if (valor) {
                    if (Grails.get(CompraService).deshabilitarCanasta(valor, usuarioActivo)) {
                        Notification.show("Canasta Cambiado", "Se cambio el estado de habilitado.", Notification.Type.TRAY_NOTIFICATION)
                    } else {
                        Notification.show("Error Canasta", "Error al cambiar el estado de habilitado.", Notification.Type.WARNING_MESSAGE)
                    }
                    actualizarTabla()
                }
            }
        })

        btnNuevoItemCanasta = new Button("Nuevo Item Canasta")
        btnNuevoItemCanasta.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent clickEvent) {
                NuevoItemCanasta nic = new NuevoItemCanasta()
                nic.addCloseListener(new Window.CloseListener() {
                    @Override
                    void windowClose(Window.CloseEvent closeEvent) {
                        actualizarTabla()
                    }
                })
                getUI().addWindow(nic)
            }
        })

        hlControles.addComponent(btnNuevo)
        hlControles.addComponent(btnDeshabilitar)
        hlControles.addComponent(btnNuevoItemCanasta)

        tablaCanastas = new Table("Canastas")
        tablaCanastas.setSizeFull()
        tablaCanastas.setSelectable(true)
        tablaCanastas.setImmediate(true)
        tablaCanastas.addContainerProperty("nombre", String.class, null, "Nombre", null, null)
        tablaCanastas.addContainerProperty("descripcion", String.class, null, "Descripcion", null, null)
        tablaCanastas.addContainerProperty("cantidad", Integer.class, null, "Cantidad Items", null, null)

        actualizarTabla()

        mainLayout.addComponent(hlControles)
        mainLayout.addComponent(tablaCanastas)
        mainLayout.setComponentAlignment(hlControles, Alignment.MIDDLE_CENTER)
    }

    private void actualizarTabla() {
        tablaCanastas.removeAllItems()
        List<Canasta> canastaList = Grails.get(CompraService).listarCanastas()
        for (Canasta c : canastaList) {
            tablaCanastas.addItem([c.nombre, c.descripcion, c.listaItems.size()] as Object[], c)
        }
    }
}
