package com.snapsoft.demo.principales

import com.snapsoft.demo.componentes.BarraEstado
import com.snapsoft.demo.componentes.Contenido
import com.vaadin.ui.Component
import com.vaadin.ui.VerticalLayout

/**
 * Created by aluis on 5/21/14.
 */
class Principal extends VerticalLayout {

    private VerticalLayout mainLayout

    private BarraEstado barraEstado
    public static Contenido contenido

    public Principal() {
        setSizeFull()
        addComponent(buildLayout())
    }

    private Component buildLayout() {
        mainLayout = new VerticalLayout()

        barraEstado = new BarraEstado()
        contenido = new Contenido()

        mainLayout.addComponent(barraEstado)
        mainLayout.addComponent(contenido)

        return mainLayout
    }


}
