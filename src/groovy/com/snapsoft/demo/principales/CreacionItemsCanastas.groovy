package com.snapsoft.demo.principales

import com.snapsoft.demo.domains.seguridad.Usuario
import com.snapsoft.demo.utileria.Constantes
import com.vaadin.server.VaadinSession
import com.vaadin.ui.Label
import com.vaadin.ui.Panel
import com.vaadin.ui.VerticalLayout

/**
 * Created by aluis on 5/22/14.
 */
class CreacionItemsCanastas extends Panel {

    private VerticalLayout mainLayout

    private Usuario usuarioActivo

    public CreacionItemsCanastas() {
        usuarioActivo = (Usuario) VaadinSession.current.session.getAttribute(Constantes.USER_LOGIN)
        setSizeFull()
        mainLayout = new VerticalLayout()
        mainLayout.setSizeFull()

        buildLayout()

        setContent(mainLayout)
    }

    private void buildLayout() {
        mainLayout.addComponent(new Label("Esto lo quitar y pones lo que necesitas"))
    }
}
