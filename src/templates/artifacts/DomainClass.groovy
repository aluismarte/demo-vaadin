@artifact.package@class @artifact.name@ {

    boolean habilitado = true
    String creadoPor = "Sistema"
    String modificadoPor = "Sistema"
    Date dateCreated
    Date lastUpdated

    static constraints = {
    }

    static mapping = {
        table ""
    }
}
