package com.snapsoft.demo.services

import com.snapsoft.demo.domains.compra.Canasta
import com.snapsoft.demo.domains.compra.ItemCanasta
import com.snapsoft.demo.domains.seguridad.Usuario
import grails.transaction.Transactional

@Transactional
class CompraService {

    public List<Canasta> listarCanastas() {
        return Canasta.findAll()
    }

    public Canasta crearCanasta(Canasta canasta, Usuario usuarioEditor) {
        if (canasta != null && !canasta.hasErrors()) {
            def canastaExiste = Canasta.get(canasta.id)
            if (canastaExiste) {
                canasta.modificadoPor = usuarioEditor.nombre
                canasta.lastUpdated = new Date()
                return canasta.merge(flush: true, failOnError: true)
            } else {
                canasta.creadoPor = usuarioEditor.nombre
                canasta.modificadoPor = usuarioEditor.nombre
                return canasta.save(flush: true, failOnError: true)
            }
        }
        return null
    }

    public Usuario deshabilitarCanasta(Canasta canasta, Usuario usuarioEditor) {
        if (canasta != null && !canasta.hasErrors()) {
            canasta.habilitado = !canasta.habilitado
            canasta.modificadoPor = usuarioEditor.nombre
            canasta.lastUpdated = new Date()
            return canasta.merge(flush: true, failOnError: true)
        }
        return null
    }

    public List<ItemCanasta> listarItemCanasta() {
        return ItemCanasta.findAll()
    }

    public ItemCanasta crearItemCanasta(ItemCanasta itemCanasta, Usuario usuarioEditor) {
        if (itemCanasta != null && !itemCanasta.hasErrors()) {
            def itemCanastaExiste = ItemCanasta.get(itemCanasta.id)
            if (itemCanastaExiste) {
                itemCanasta.modificadoPor = usuarioEditor.nombre
                itemCanasta.lastUpdated = new Date()
                return itemCanasta.merge(flush: true, failOnError: true)
            } else {
                itemCanasta.creadoPor = usuarioEditor.nombre
                itemCanasta.modificadoPor = usuarioEditor.nombre
                return itemCanasta.save(flush: true, failOnError: true)
            }
        }
        return null
    }

    public ItemCanasta deshabilitarItemCanasta(ItemCanasta itemCanasta, Usuario usuarioEditor) {
        if (itemCanasta != null && !itemCanasta.hasErrors()) {
            itemCanasta.habilitado = !itemCanasta.habilitado
            itemCanasta.modificadoPor = usuarioEditor.nombre
            itemCanasta.lastUpdated = new Date()
            return itemCanasta.merge(flush: true, failOnError: true)
        }
        return null
    }
}
