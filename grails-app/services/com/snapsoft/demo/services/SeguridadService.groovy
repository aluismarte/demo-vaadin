package com.snapsoft.demo.services

import com.snapsoft.demo.domains.seguridad.Usuario
import grails.transaction.Transactional

@Transactional
class SeguridadService {

    public Usuario autentificarUsuario(String usuario, String password) {
        return Usuario.findByUsernameAndPasswordAndHabilitado(usuario, password.encodeAsSHA256(), true)
    }

    public Usuario crearUsuario(Usuario usuario, Usuario usuarioEditor) {
        if (usuario != null && !usuario.hasErrors()) {
            def usuarioExiste = Usuario.get(usuario.id)
            if (usuarioExiste) {
                usuario.modificadoPor = usuarioEditor.nombre
                usuario.lastUpdated = new Date()
                return usuario.merge(flush: true, failOnError: true)
            } else {
                usuario.creadoPor = usuarioEditor.nombre
                usuario.modificadoPor = usuarioEditor.nombre
                return usuario.save(flush: true, failOnError: true)
            }
        }
        return null
    }

    public Usuario deshabilitarUsuario(Usuario usuario, Usuario usuarioEditor) {
        if (usuario != null && !usuario.hasErrors()) {
            usuario.habilitado = !usuario.habilitado
            usuario.modificadoPor = usuarioEditor.nombre
            usuario.lastUpdated = new Date()
            return usuario.merge(flush: true, failOnError: true)
        }
        return null
    }

    public List<Usuario> listarUsuarios() {
        return Usuario.findAll()
    }
}
