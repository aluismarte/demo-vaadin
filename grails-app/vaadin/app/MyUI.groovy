package app

import com.snapsoft.demo.componentes.Login
import com.snapsoft.demo.domains.seguridad.Usuario
import com.snapsoft.demo.interfaces.ICallBackUsuario
import com.snapsoft.demo.principales.Principal
import com.vaadin.server.VaadinRequest
import com.vaadin.ui.HorizontalLayout
import com.vaadin.ui.UI

/**
 * @author Aluis 2014-05-10
 */
class MyUI extends UI {

    private HorizontalLayout mainLayout
    private Login login;

    private ICallBackUsuario callBackUsuario
    private Principal principal
    private Boolean salto = false

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        mainLayout = new HorizontalLayout()
        mainLayout.setSizeFull()
        mainLayout.setSpacing(true)
        mainLayout.addStyleName("fondo")
        setContent(mainLayout)

        instanciarCallBack()

        login = new Login(callBackUsuario)
        addWindow(login)
        if(salto) {
            login.close()
        }
    }

    private void instanciarCallBack() {
        callBackUsuario = new ICallBackUsuario() {
            @Override
            void onDialogResult(Usuario usuario) {
                principal = new Principal()
                setContent(principal)
                salto = true
            }
        }
    }
}
