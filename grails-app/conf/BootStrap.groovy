import com.snapsoft.demo.domains.seguridad.Usuario

class BootStrap {

    def init = { servletContext ->
        Usuario.findByUsername("root") ?: new Usuario(username: "root", password: "1234".encodeAsSHA256(), nombre: "Root", apellido: "Maximo", administrador: true).save(flush: true, failOnError: true)
        Usuario.findByUsername("test") ?: new Usuario(username: "test", password: "1234".encodeAsSHA256(), nombre: "Test", apellido: "Usuario", administrador: false).save(flush: true, failOnError: true)
    }
    def destroy = {
    }
}
