package com.snapsoft.demo.domains.seguridad

class Usuario {

    String username
    String password // Esto es sha256

    String nombre
    String apellido

    Boolean administrador

    boolean habilitado = true
    String creadoPor = "Sistema"
    String modificadoPor = "Sistema"
    Date dateCreated
    Date lastUpdated

    static constraints = {
    }

    static mapping = {
        table "seg_usuarios"
    }
}
