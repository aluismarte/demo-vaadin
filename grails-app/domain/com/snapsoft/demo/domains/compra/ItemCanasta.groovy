package com.snapsoft.demo.domains.compra

class ItemCanasta {

    String nombre
    String descripcion

    boolean habilitado = true
    String creadoPor = "Sistema"
    String modificadoPor = "Sistema"
    Date dateCreated
    Date lastUpdated

    static constraints = {
        descripcion nullable: true
    }

    static mapping = {
        table "com_item"
    }
}
