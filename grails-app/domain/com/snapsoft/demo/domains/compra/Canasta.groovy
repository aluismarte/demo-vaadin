package com.snapsoft.demo.domains.compra

class Canasta {

    String nombre
    String descripcion

    static hasMany = [listaItems: ItemCanasta]

    boolean habilitado = true
    String creadoPor = "Sistema"
    String modificadoPor = "Sistema"
    Date dateCreated
    Date lastUpdated

    static constraints = {
        descripcion nullable: true
    }

    static mapping = {
        table "com_canasta"
        listaItems lazy: false
    }
}
